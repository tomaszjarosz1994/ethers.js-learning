import {ethers} from "ethers";
import {INFURA_ID} from '../consts.js';

const provider = new ethers.providers.JsonRpcProvider(`https://mainnet.infura.io/v3/${INFURA_ID}`)

//https://etherscan.io/address/0xf977814e90da44bfa03b6295a0616a897441acec
const address = '0xf977814e90da44bfa03b6295a0616a897441acec'

const main = async () => {
    const balance = await provider.getBalance(address)
    console.log(`Balance of ${address}: ${ethers.utils.formatEther(balance)} ETH\n`)
}

main()