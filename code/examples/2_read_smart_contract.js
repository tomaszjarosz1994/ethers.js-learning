import {ethers} from "ethers";
import {INFURA_ID} from '../consts.js';

const provider = new ethers.providers.JsonRpcProvider(`https://mainnet.infura.io/v3/${INFURA_ID}`)
const USDT_contract_address = '0xdac17f958d2ee523a2206206994597c13d831ec7';
const ERC20_ABI = [
    "function name() view returns (string)",
    "function symbol() view returns (string)",
    "function totalSupply() view returns (uint256)",
    "function balanceOf(address) view returns (uint)",
];

const contract = new ethers.Contract(USDT_contract_address, ERC20_ABI, provider);

const main = async () => {
    const name = await contract.name()
    const symbol = await contract.symbol()
    const totalSupply = await contract.totalSupply()
    const addressToCheck = '0xf977814e90da44bfa03b6295a0616a897441acec'
    const balanceOf = await contract.balanceOf(addressToCheck)
    const formattedBalance = ethers.utils.formatEther(balanceOf)


    console.log(`\n`)
    console.log(`Reading contract: ${USDT_contract_address}`)
    console.log(`name: ${name}`)
    console.log(`symbol: ${symbol}`)
    console.log(`totalSupply: ${totalSupply}`)
    console.log(`balanceOf ${addressToCheck}: ${formattedBalance} USDT` )
    console.log(`\n`)
}
main()
