import {ethers} from "ethers";
import {INFURA_ID} from '../consts.js';

const provider = new ethers.providers.JsonRpcProvider(`https://kovan.infura.io/v3/${INFURA_ID}`)

const accountFrom = ''
const privateKeyFrom = ''
const accountTo = ''

const wallet = new ethers.Wallet(privateKeyFrom, provider)

const main = async () => {
    const senderBalanceBefore = await provider.getBalance(accountFrom)
    const receiverBalanceBefore = await provider.getBalance(accountTo)

    console.log(`\n`)
    console.log(`Sender balance before: ${ethers.utils.formatEther(senderBalanceBefore)}`)
    console.log(`Receiver balance before: ${ethers.utils.formatEther(receiverBalanceBefore)}\n`)

    const tx = await wallet.sendTransaction({
        to: accountTo,
        value: ethers.utils.parseEther("0.005")
    })

    await tx.wait()
    console.log(tx)

    const senderBalanceAfter = await provider.getBalance(accountFrom)
    const receiverBalanceAfter = await provider.getBalance(accountTo)

    console.log(`\n`)
    console.log(`Sender balance after: ${ethers.utils.formatEther(senderBalanceAfter)}`)
    console.log(`Receiver balance after: ${ethers.utils.formatEther(receiverBalanceAfter)}\n`)
}

main()
