import {ethers} from "ethers";
import {INFURA_ID} from '../consts.js';

const provider = new ethers.providers.JsonRpcProvider(`https://kovan.infura.io/v3/${INFURA_ID}`)

const accountFrom = ''
const privateKeyFrom = ''
const accountTo = ''

const wallet = new ethers.Wallet(privateKeyFrom, provider)

const ERC20_ABI = [
    "function balanceOf(address) view returns (uint)",
    "function transfer(address to, uint amount) returns (bool)",
];

const address = ''
const contract = new ethers.Contract(address, ERC20_ABI, provider)

const main = async () => {
    const balance = await contract.balanceOf(accountFrom)

    console.log(`\n`)
    console.log(`Reading from ${address}\n`)
    console.log(`Balance of sender: ${balance}\n`)

    const contractWithWallet = contract.connect(wallet)

    const tx = await contractWithWallet.transfer(accountTo, balance)
    await tx.wait()

    console.log(tx)

    const balanceOfSender = await contract.balanceOf(accountFrom)
    const balanceOfReceiver = await contract.balanceOf(accountTo)

    console.log(`\n`)
    console.log(`Balance of sender: ${balanceOfSender}`)
    console.log(`Balance of receiver: ${balanceOfReceiver}\n`)
}

main()
