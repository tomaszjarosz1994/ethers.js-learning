import {ethers} from "ethers";
import {INFURA_ID} from '../consts.js';

const provider = new ethers.providers.JsonRpcProvider(`https://mainnet.infura.io/v3/${INFURA_ID}`)

const ERC20_ABI = [
    "function name() view returns (string)",
    "function symbol() view returns (string)",
    "function totalSupply() view returns (uint256)",
    "function balanceOf(address) view returns (uint)",

    "event Transfer(address indexed from, address indexed to, uint amount)"
];
const USDT_contract_address = '0xdac17f958d2ee523a2206206994597c13d831ec7';
const contract = new ethers.Contract(USDT_contract_address, ERC20_ABI, provider)

const main = async () => {
    const block = await provider.getBlockNumber()
    const events = await contract.queryFilter('Transfer', block - 10, block)
    console.log(events)
}

main()
