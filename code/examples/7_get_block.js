import {ethers} from "ethers";
import {INFURA_ID} from '../consts.js';

const provider = new ethers.providers.JsonRpcProvider(`https://mainnet.infura.io/v3/${INFURA_ID}`)

const main = async () => {
    const blockNumber = await provider.getBlockNumber()
    console.log(`\n`)
    console.log(`Block Number: ${blockNumber}\n`)

    const block = await provider.getBlock(blockNumber)

    console.log(block)

    const { transactions } = await provider.getBlockWithTransactions(blockNumber)

    console.log(`\n`)
    console.log(`Logging first transaction in block:\n`)
    console.log(transactions[0])
}

main()
